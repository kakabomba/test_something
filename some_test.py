from tensorflow import test
import gin.tf

from some_class import ClassToTest

class GinFlagTest(test.TestCase):

  def test_gin(self):

    gin.parse_config_files_and_bindings(['some.gin'], bindings=[])

    o = ClassToTest(b=5)

    self.assertEqual(o._a, 3)
    self.assertEqual(o._b, 5)

if __name__ == '__main__':
  test.main()
