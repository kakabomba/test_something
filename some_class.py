import gin.tf

@gin.configurable
class ClassToTest:
  _a = None
  _b = None

  def __init__(self, a=1, b=2):
    self._a = a
    self._b = b
    print(f"a={self._a}, b={self._b}")
